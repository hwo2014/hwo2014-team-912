import json
import socket
import sys
import math

# This is Python 3
class Bot(object):
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

        self.color = ""

        self.is_out = False

        self.current_lap = 0
        self.current_lane = 0

        self.turbo_available_until = 0

        self.race_track = {}
        self.race_cars = {}
        self.race_session = {}

        # should also contain max (and min?) pieces - we need to know if an opponent is in segment
        self.race_track_slices = [] # contains dictionary: with first_radius, total angles, last maxangle, last speed - to adjust turn speed on the fly
        self.race_track_segments = [] # contains dictionary: with total angles

        self.car_positions = {}

        self.game_tick = 0

        #self.turbo_ticks = 0

        self.has_changed_distance = False

        self.has_started = False
        self.initial_acc = 0

        self.after_start = False # asking whether this is one tick after start?
        self.second_acc = 0

        self.max_speed = 0 # WATCH OUT WHEN CALCULATING MAX_TURN_SPEEDS WITH THIS AT 0

        self.last_throttle = 0
        self.before_last_throttle = 0
        self.last_switch = ""

        self.previous_tick_piece = 0
        self.current_tick_piece = 0

        self.previous_tick_slice = 0
        self.current_tick_slice = 0

        self.current_tick_segment = 0

        self.previous_tick_position = 0
        self.current_tick_position = 0

        self.previous_tick_speed = 0
        self.current_tick_speed = 0

        self.current_tick_slice_position = 0

        #self.previous_tick_acc = 0
        self.current_tick_acc = 0

        self.previous_tick_slip_angle = 0
        self.current_tick_slip_angle = 0

        self.previous_tick_slip_angle_speed = 0
        self.current_tick_slip_angle_speed = 0

        #self.previous_tick_slip_angle_acc = 0
        self.current_tick_slip_angle_acc = 0

        self.car_mass = 0
        self.tyre_friction = 0

    # message
    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type,
                              "data": data}))

    # send message
    def send(self, msg):
        #changed from send to sendall
        self.socket.sendall(msg + "\n")

    def create_race(self, race_name):
        print("Creating race at: {0}".format(race_name))
        return self.msg("createRace", {"botId":{"name": self.name,
                                                "key": self.key},
                                       "trackName":race_name,
                                       "password":"abc",
                                       "carCount":1})

    def join_race(self, race_name):
        print("Joining race at: {0}".format(race_name))
        return self.msg("joinRace", {"botId":{"name": self.name,
                                              "key": self.key},
                                     "trackName":race_name,
                                     "carCount":1})

    def join(self):
        print("Joining")
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        print("Throttle: {0}".format(throttle))
        self.last_throttle = throttle
        self.msg("throttle", throttle)
        self.msg("throttle", throttle) # making sure

    def switch_lane(self, direction): # needs a string with "Right" or "Left"
        print("Switching lanes: {0}".format(direction))
        self.last_switch = direction
        self.msg("switchLane", direction)

    def turbo(self):
        print("Activating turbo (go go go!)")
        self.msg("turbo", "Go go go!")

    def ping(self):
        print("Ping")
        self.msg("ping", {})

    def run(self):
        #self.join_race("usa")
        #self.join_race("germany")
        #self.join_race("keimola")
        #self.join_race("france")
        self.join()
        self.msg_loop()

    def simple_run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined:")
        print(data['name'])
        print(data['key'])
        print("Tick: {0}".format(self.game_tick))
        self.ping()

    def on_your_car(self, data):
        print("Car:")
        print(data['name'])
        self.color = data['color']
        print(data['color'])
        print("Tick: {0}".format(self.game_tick))
        self.ping()

    def on_game_init(self, data):
        print("Game initialized")
        print("Tick: {0}".format(self.game_tick))
        self.race_track = data['race']['track']
        self.race_cars = data['race']['cars']
        self.race_session = data['race']['raceSession']

        # ---------------------------------------------------------------------
        # TRACK PREPROCESSING:

        # DETERMINE SLICE AND SEGMENT OF EACH PIECE
        # SLICE = A CONTINUOUS STRAIGHT/CURVED PART OF TRACK (CONTINUES THROUGH SWITCHES, BREAK AT INFLECTION POINTS)
        # SEGMENT = A CONTINUOUS PART OF TRACK BETWEEN SWITCHES (INCLUDES THE LAST, NOT THE FIRST ONE)
        current_slice = 0
        max_slice = 0

        current_segment = 0
        max_segment = 0

        prev_is_straight = True
        prev_is_switch = False
        prev_is_right = False # right turn = positive angle

        for piece in self.race_track['pieces']:
            current_length = 0 # NOT FOR CURVED
            current_angle = 0 # NOT FOR STRAIGHT
            current_radius = 0 # NOT FOR STRAIGHT

            current_is_straight = True
            if 'radius' in piece:
                current_is_straight = False
                current_angle = piece['angle']
                current_radius = piece['radius']
            else:
                current_length = piece['length']

            current_is_switch = False
            if 'switch' in piece: current_is_switch = True

            current_is_right = False
            if current_angle > 0: current_is_right = True

            if (prev_is_straight != current_is_straight) or (prev_is_right != current_is_right):
                current_slice += 1
                max_slice = current_slice

            if prev_is_switch:
                current_segment += 1
                max_segment = current_segment

            piece['slice'] = current_slice
            #print(str(current_slice)+", "+str(len(self.race_track_slices)))
            if current_slice >= len(self.race_track_slices):
                if current_is_straight:
                    #print("appending straight")
                    self.race_track_slices.append({'length':current_length})
                else:
                    #print("appending turn")
                    self.race_track_slices.append({'angle':current_angle,
                                                   'firstRadius':current_radius})

            else:
                if current_is_straight:
                    #print("rewriting straight")
                    self.race_track_slices[current_slice]['length'] += current_length
                else:
                    #print("rewriting turn")
                    self.race_track_slices[current_slice]['angle'] += current_angle

            piece['segment'] = current_segment
            #print(str(current_segment)+", "+str(len(self.race_track_segments)))
            if current_segment >= len(self.race_track_segments):
                if current_is_straight:
                    pass
                else:
                    #print("appending")
                    self.race_track_segments.append({'angle':current_angle})
            else:
                if current_is_straight:
                    pass
                else:
                    #print("rewriting")
                    self.race_track_segments[current_segment]['angle'] += current_angle

            prev_is_straight = current_is_straight
            prev_is_switch = current_is_switch
            prev_is_right = current_is_right

        if max_slice > 0:
            for piece in self.race_track['pieces']: # MAX SLICE AND MAX SEGMENTS ARE CONTINUOUS WITH 0
                if piece['slice'] == max_slice: piece['slice'] = 0
            max_slice_data = self.race_track_slices.pop(-1)
            if 'angle' in max_slice_data:
                self.race_track_slices[0]['angle'] += max_slice_data['angle']
                # no need to worry about starting radius since start is at a straight
            if 'length' in max_slice_data:
                self.race_track_slices[0]['length'] += max_slice_data['length']
                self.race_track_slices[0]['startLength'] = max_slice_data['length']

        if max_segment > 0:
            for piece in self.race_track['pieces']:
                if piece['segment'] == max_segment: piece['segment'] = 0
            max_segment_data = self.race_track_segments.pop(-1)
            if 'angle' in max_segment_data:
                self.race_track_segments[0]['angle'] += max_segment_data['angle']

        # ---------------------------------------------------------------------

        self.ping()

    def on_game_start(self, data):
        print("Race started")
        print("Tick: {0}".format(self.game_tick))
        self.throttle(1)

    def on_car_positions(self, data, game_tick):
        print("\nCar positions received")
        if not self.is_out:
            self.car_positions = data


            # -----------------------------------------------------------------
            # DATA PROCESSING:

            # TICK
            self.game_tick = game_tick
            print("Tick: {0}".format(self.game_tick))

            # PIECE
            self.previous_tick_piece = self.current_tick_piece
            self.current_tick_piece = self._get_car_data()['piecePosition']['pieceIndex']
            current_tick_piece_info = self.race_track['pieces'][self.current_tick_piece]
            print("Piece: {0} ({1})".format(self.current_tick_piece, current_tick_piece_info))

            # LANE
            self.current_lane = self._get_car_data()['piecePosition']['lane']['endLaneIndex']
            print("Lane: {0}".format(self.current_lane))

            # SLICE
            self.previous_tick_slice = self.current_tick_slice
            self.current_tick_slice = self.race_track['pieces'][self.current_tick_piece]['slice']
            #print("Slice: {0}".format(self.current_tick_slice))

            # SEGMENT
            self.current_tick_segment = self.race_track['pieces'][self.current_tick_piece]['segment']
            #print("Segment: {0}".format(self.current_tick_segment))

            # POSITION
            self.previous_tick_position = self.current_tick_position
            self.current_tick_position = self._get_car_data()['piecePosition']['inPieceDistance']
            print("Position: {0}".format(self.current_tick_position))

            # SPEED
            self.previous_tick_speed = self.current_tick_speed
            if self.previous_tick_piece == self.current_tick_piece:
                self.current_tick_speed = self.current_tick_position-self.previous_tick_position
            else: # ASSUMES ONLY ONE CHANGE CAN BE DONE - NOT TOO FAST OR SHORT PIECES - doesn't count switches
                if 'radius' in self.race_track['pieces'][self.previous_tick_piece]:
                    radius = self.race_track['pieces'][self.previous_tick_piece]['radius']
                    angle = self.race_track['pieces'][self.previous_tick_piece]['angle']
                    angle_sign = 1
                    if angle < 0: angle_sign = -1
                    
                    lane_index = self._get_car_data()['piecePosition']['lane']['endLaneIndex'] # THE END - NO CHANGES ASSUMED
                    lane_offset = (-1)*angle_sign*self.race_track['lanes'][lane_index]['distanceFromCenter'] # NORMALIZING LANE OFFSETS

                    first_piece_length = (2*math.pi*(radius+lane_offset))*(math.fabs(angle)/360)

                    first_piece_length_left = first_piece_length-self.previous_tick_position

                    self.current_tick_speed = first_piece_length_left+self.current_tick_position
                else:
                    first_piece_length = self.race_track['pieces'][self.previous_tick_piece]['length']
                    first_piece_length_left = first_piece_length-self.previous_tick_position

                    self.current_tick_speed = first_piece_length_left+self.current_tick_position
            print("Speed: {0}".format(self.current_tick_speed))

            # SLICE POSITION
            if 'length' in self.race_track_slices[self.current_tick_slice]:
                if self.previous_tick_slice != self.current_tick_slice:
                    self.current_tick_slice_position = self.current_tick_position
                else:
                    self.current_tick_slice_position += self.current_tick_speed
                if not self.has_changed_distance:
                    self.current_tick_slice_position += self.race_track_slices[0]['startLength']
                    self.has_changed_distance = True
                print("Slice length: {0}".format(self.race_track_slices[self.current_tick_slice]['length']))

            # ACCELERATION
            #self.previous_tick_acc = self.current_tick_acc
            self.current_tick_acc = self.current_tick_speed-self.previous_tick_speed
            print("Acceleration: {0}".format(self.current_tick_acc))

            # GATHERING START DATA
            if self.has_started and (not self.after_start):
                self.after_start = True
                self.second_acc = self.current_tick_acc

                self.max_speed = (self.initial_acc**2)/(self.initial_acc-self.second_acc) # MAX SPEED CALCULATION
                print("Initial acc: {0}".format(self.initial_acc))
                print("Second acc: {0}".format(self.second_acc))
                print("Car max speed: {0}".format(self.max_speed))

            if (not self.has_started) and (self.previous_tick_speed == 0) and (self.current_tick_speed > 0):
                self.has_started = True
                self.initial_acc = self.current_tick_acc

            # SLIP ANGLE
            self.previous_tick_slip_angle = self.current_tick_slip_angle
            self.current_tick_slip_angle = self._get_car_data()['angle']
            print("Slip angle: {0}".format(self.current_tick_slip_angle))

            # SLIP ANGLE SPEED
            self.previous_tick_slip_angle_speed = self.current_tick_slip_angle_speed
            self.current_tick_slip_angle_speed = self.current_tick_slip_angle-self.previous_tick_slip_angle
            print("Slip angle speed: {0}".format(self.current_tick_slip_angle_speed))

            # SLIP ANGLE ACCELERATION
            #self.previous_tick_slip_angle_acc = self.current_tick_slip_angle_acc
            self.current_tick_slip_angle_acc = self.current_tick_slip_angle_speed-self.previous_tick_slip_angle_speed
            print("Slip angle acc: {0}".format(self.current_tick_slip_angle_acc))

            # -----------------------------------------------------------------
            # ADDITIONAL PROCESSING:

            if ('angle' in self.race_track_slices[self.current_tick_slice]) and (self.previous_tick_slice != self.current_tick_slice):
                self.race_track_slices[self.current_tick_slice]['enterSpeed'] = self.current_tick_speed
                self.race_track_slices[self.current_tick_slice]['maxAngle'] = math.fabs(self.current_tick_slip_angle)

            if ('maxAngle' in self.race_track_slices[self.current_tick_slice]) and (math.fabs(self.current_tick_slip_angle) > math.fabs(self.race_track_slices[self.current_tick_slice]['maxAngle'])):
                self.race_track_slices[self.current_tick_slice]['maxAngle'] = math.fabs(self.current_tick_slip_angle)

            #print(self.race_track_slices[self.current_tick_slice])

            # -----------------------------------------------------------------
            # ALGORITHM:

            FULL_THROTTLE = 1

            same_throttle = False
            same_switch = False

            if not self.after_start:
                self.throttle(1)

            else:
                #print(self.race_track_slices[self.current_tick_slice])
                if 'length' in self.race_track_slices[self.current_tick_slice]: # if current slice is straight
                    next_slice = self.race_track_slices[(self.current_tick_slice+1)%len(self.race_track_slices)]
                    max_turn_speed = self._max_turn_speed(next_slice)
                    #print("Max turn speed: {0}".format(max_turn_speed))

                    average_acceleration = (self.current_tick_speed+max_turn_speed)*(-1.0)*(1.0/2.0)*(self.initial_acc/self.max_speed)
                    #print("Average acceleration: {0}".format(average_acceleration))

                    slowdown_time = (max_turn_speed-(self.current_tick_speed+2*self.current_tick_acc))/((1.15)*average_acceleration) # compensating for delay
                    slowdown_time = math.ceil(slowdown_time) # to give some leeway - delay of 1
                    #print("Slowdown time: {0}".format(slowdown_time))

                    slowdown_distance = ((max_turn_speed*slowdown_time) - ((1/2)*average_acceleration*(slowdown_time**2))) # *(1+((self.current_tick_speed-4)/4)) # to compensate time delay
                    print("Slowdown distance: {0}".format(slowdown_distance))
                    # underestimates

                    distance_until_turn = self.race_track_slices[self.current_tick_slice]['length'] - self.current_tick_slice_position
                    print("Distance until turn: {0}".format(distance_until_turn))
                    if distance_until_turn >= slowdown_distance+3*self.current_tick_speed:
                        #print("Should change to 1")
                        if self.last_throttle != FULL_THROTTLE:
                            self.throttle(FULL_THROTTLE)
                            """if self.before_last_throttle != FULL_THROTTLE:
                                self.throttle(FULL_THROTTLE)
                                self.before_last_throttle = self.last_throttle"""
                        else:
                            same_throttle = True

                        if self.turbo_available_until >= self.game_tick and distance_until_turn >= slowdown_distance+7.5*self.current_tick_speed:
                            self.turbo()

                    else:
                        #print("Should change to 0")
                        if self.last_throttle != 0:
                            self.throttle(0)
                            """if self.before_last_throttle != 0:
                                self.throttle(0)
                                self.before_last_throttle = self.last_throttle"""
                        else:
                            same_throttle = True

                else: # if current slice is curved
                    max_slip_angle_speed = (55 - math.fabs(self.current_tick_slip_angle))/25 - math.fabs(self.current_tick_slip_angle_speed)-math.fabs(self.current_tick_slip_angle_acc) # time delay of 1
                    max_slip_angle_speed -= self.current_tick_slip_angle_acc
                    print("Max slip angle speed: {0}".format(max_slip_angle_speed))
                    max_slip_angle_acc = max_slip_angle_speed/5
                    print("Max slip angle acc: {0}".format(max_slip_angle_acc))
                    if (math.fabs(self.current_tick_slip_angle_speed) < max_slip_angle_speed):
                        if (self.current_tick_slip_angle > 0 and self.current_tick_slip_angle_acc < max_slip_angle_acc) or (self.current_tick_slip_angle <= 0 and self.current_tick_slip_angle_acc > -(max_slip_angle_acc)): # NOT CHANGING TOO FAST
                            #print("Should change to 1")
                            if self.last_throttle != FULL_THROTTLE:
                                self.throttle(FULL_THROTTLE)
                                """if self.before_last_throttle != FULL_THROTTLE:
                                    self.throttle(FULL_THROTTLE)
                                    self.before_last_throttle = self.last_throttle"""
                            else:
                                same_throttle = True
                        else:
                            #print("Should change to 0")
                            if self.last_throttle != 0:
                                self.throttle(0)
                                """if self.before_last_throttle != 0:
                                    self.throttle(0)
                                    self.before_last_throttle = self.last_throttle"""
                            else:
                                same_throttle = True
                    else:
                        #print("Should change to 0")
                        if self.last_throttle != 0:
                            self.throttle(0)
                            """if self.before_last_throttle != 0:
                                self.throttle(0)
                                self.before_last_throttle = self.last_throttle"""
                        else:
                            same_throttle = True

                if same_throttle:
                    # IF DESIRABLE TO CHANGE FOR NEXT, SEND THE MSG
                    next_segment_angle = self.race_track_segments[(self.current_tick_segment+1)%len(self.race_track_segments)]['angle']
                    if next_segment_angle < 0: # NEGATIVE MEANS LEFT-TURN
                        if self.last_switch != "Left" and self.current_lane != 0: # 0 is the leftmost
                            self.switch_lane("Left")
                            #self.last_switch = "Left"
                            #print("Will switch left")
                        elif self.current_lane == 0:
                            self.throttle(self.last_throttle)
                        else:
                            same_switch = True
                        
                    elif next_segment_angle > 0: # POSITIVE MEANS RIGHT-TURN
                        if self.last_switch != "Right" and self.current_lane != len(self.race_track['lanes']):
                            self.switch_lane("Right")
                            #self.last_switch = "Right"
                            #print("Will switch right")
                        elif self.current_lane == len(self.race_track['lanes']):
                            self.throttle(self.last_throttle)
                        else:
                            same_switch = True

                    else:
                        self.ping()
                else:
                    pass
                    """print("Tick: {0}".format(self.game_tick))
                    print("Piece: {0} ({1})".format(self.current_tick_piece, current_tick_piece_info))
                    #print("Slice: {0}".format(self.current_tick_slice))
                    #print("Segment: {0}".format(self.current_tick_segment))
                    print("Position: {0}".format(self.current_tick_position))
                    print("Speed: {0}".format(self.current_tick_speed))
                    print("Acceleration: {0}".format(self.current_tick_acc))
                    print("Slip angle: {0}".format(self.current_tick_slip_angle))
                    print("Slip angle speed: {0}".format(self.current_tick_slip_angle_speed))
                    print("Slip angle acc: {0}".format(self.current_tick_slip_angle_acc))"""

                if not same_switch:
                    pass
                else:
                    self.ping()

            # -----------------------------------------------------------------

        else:
            self.ping()

    def _get_car_data(self):
        i = 0
        while self.car_positions[i]['id']['color'] != self.color: continue
        return self.car_positions[i]

    # gives 3.5 for 50, 5 for 100, 8.5 for 200
    def _max_turn_speed(self, next_slice): # this should record distance_to_turn and act accordingly
        #print(next_slice)

        check_lap = self.current_lap
        current_slice = self.race_track_slices[self.current_tick_slice]
        if self.current_tick_slice == 0 and self.current_tick_slice_position <= current_slice['startLength']+self.current_tick_speed: # lap info arrives after car positions
            check_lap += 1
        #print("Current slice position: {0}".format(self.current_tick_slice_position))
        #print("Check lap: {0}".format(check_lap))

        if 'maxAngle' in next_slice:
            if next_slice['maxAngle'] < 10:
                add_coefficient = 1 + 0.5*((10-next_slice['maxAngle'])/10) # 1.5
                if 'addToMaxSpeed' in next_slice and 'lastChangingLap' in next_slice:
                    if next_slice['lastChangingLap'] != check_lap:
                        next_slice['addToMaxSpeed'] = add_coefficient
                        next_slice['lastChangingLap'] = check_lap
                else:
                    next_slice['addToMaxSpeed'] = add_coefficient
                    next_slice['lastChangingLap'] = check_lap
                #print("Max turn speed +2.5")
            elif next_slice['maxAngle'] < 20:
                add_coefficient = 0.5 + 0.5*((20-next_slice['maxAngle'])/10) # 1
                if 'addToMaxSpeed' in next_slice and 'lastChangingLap' in next_slice:
                    if next_slice['lastChangingLap'] != check_lap:
                        next_slice['addToMaxSpeed'] = add_coefficient
                        next_slice['lastChangingLap'] = check_lap
                else:
                    next_slice['addToMaxSpeed'] = add_coefficient
                    next_slice['lastChangingLap'] = check_lap
                #print("Max turn speed +2")
            elif next_slice['maxAngle'] < 30:
                add_coefficient = 0.25 + 0.25*((30-next_slice['maxAngle'])/10) # 0.5
                if 'addToMaxSpeed' in next_slice and 'lastChangingLap' in next_slice:
                    if next_slice['lastChangingLap'] != check_lap:
                        next_slice['addToMaxSpeed'] = add_coefficient
                        next_slice['lastChangingLap'] = check_lap
                else:
                    next_slice['addToMaxSpeed'] = add_coefficient
                    next_slice['lastChangingLap'] = check_lap
                #print("Max turn speed +1.5")
            elif next_slice['maxAngle'] < 40:
                add_coefficient = 0 + 0.25*((40-next_slice['maxAngle'])/10) # 0.25
                if 'addToMaxSpeed' in next_slice and 'lastChangingLap' in next_slice:
                    if next_slice['lastChangingLap'] != check_lap:
                        next_slice['addToMaxSpeed'] = add_coefficient
                        next_slice['lastChangingLap'] = check_lap
                else:
                    next_slice['addToMaxSpeed'] = add_coefficient
                    next_slice['lastChangingLap'] = check_lap
                #print("Max turn speed +1")
            elif next_slice['maxAngle'] < 50:
                add_coefficient = 0 - 0.25*((50-next_slice['maxAngle'])/10) # 0
                if 'addToMaxSpeed' in next_slice and 'lastChangingLap' in next_slice:
                    if next_slice['lastChangingLap'] != check_lap:
                        next_slice['addToMaxSpeed'] = add_coefficient
                        next_slice['lastChangingLap'] = check_lap
                else:
                    next_slice['addToMaxSpeed'] = add_coefficient
                    next_slice['lastChangingLap'] = check_lap
                #print("Max turn speed +0.5")
            elif next_slice['maxAngle'] >= 50:
                add_coefficient = -0.25 - 0.25*((60-next_slice['maxAngle'])/10) # -0.25
                if 'addToMaxSpeed' in next_slice and 'lastChangingLap' in next_slice:
                    if next_slice['lastChangingLap'] != check_lap:
                        next_slice['addToMaxSpeed'] = -0.25
                        next_slice['lastChangingLap'] = check_lap
                else:
                    next_slice['addToMaxSpeed'] = -0.25
                    next_slice['lastChangingLap'] = check_lap
                #print("Max turn speed -0.25")

        max_speed = 0

        # max_speed -= math.fabs(self.current_tick_slip_angle/45)

        if 'enterSpeed' in next_slice:
            max_speed = next_slice['enterSpeed']
        else:
            max_speed = 2 + 1.5*(next_slice['firstRadius']/50)# - math.fabs(self._get_turn_angle()/180)
            # too optimistic for sharp turns, too pesimistic for mild turns
            # record slip angles for turns and adjust in following rounds?
            # the const and multiplier are the best candidates
        print("Original max speed: {0}".format(max_speed))

        if 'addToMaxSpeed' in next_slice:
            max_speed += next_slice['addToMaxSpeed']
        print("New max speed: {0}".format(max_speed))

        #print(next_slice)
        return max_speed

    def on_turbo_available(self, data):
        # USE THE TURBO AND CHANGE THE ACCELERATION FACTORS
        self.turbo_ticks = data['turboDurationTicks']
        #self.turbo_factor = data['turboFactor']

        print("Turbo is available!")
        self.turbo_available_until = self.game_tick+data['turboDurationTicks']-1; # wait until a straight
        #self.turbo() # just use it

    def on_turbo_start(self, data):
        print("Turbo starting")
        if self.last_throttle != 0: self.throttle(self.last_throttle)
        else: self.ping()

    def on_turbo_end(self, data):
        print("Turbo ending")
        if self.last_throttle != 0: self.throttle(self.last_throttle)
        else: self.ping()

    def on_crash(self, data):
        if data['color'] == self.color:
            print("You crashed")
            self.is_out = True
        else:
            print("Someone else crashed")
        print("Tick: {0}".format(self.game_tick))
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        print("Tick: {0}".format(self.game_tick))
        self.ping()

    def on_tournament_end(self, data):
        print("Tournament ended")
        print("Tick: {0}".format(self.game_tick))
        self.ping()

    def on_spawn(self, data):
        if data['color'] == self.color:
            print("You spawned")
            self.is_out = False
            self.throttle(1)
        else:
            print("Someone else spawned")
            self.ping()
        print("Tick: {0}".format(self.game_tick))

    def on_lap_finished(self, data):
        if data['car']['color'] == self.color:
            self.current_lap += 1
            print("You completed a lap")
        else:
            print("Someone else completed a lap")
        print("Tick: {0}".format(self.game_tick))
        self.ping()

    def on_dnf(self, data):
        if data['car']['color'] == self.color: print("You were disqualified")
        else: print("Someone else was disqualified")
        print("Tick: {0}".format(self.game_tick))
        self.ping()

    def on_finish(self, data):
        if data['color'] == self.color: print("You finished")
        else: print("Someone else finished")
        print("Tick: {0}".format(self.game_tick))
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        print("Tick: {0}".format(self.game_tick))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'yourCar': self.on_your_car, #
            'gameInit': self.on_game_init, #
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'turboAvailable': self.on_turbo_available, #
            'turboStart': self.on_turbo_start, #
            'turboEnd': self.on_turbo_end, #
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'tournamentEnd': self.on_tournament_end, #
            'spawn': self.on_spawn, #
            'lapFinished': self.on_lap_finished, #
            'dnf': self.on_dnf, #
            'finish': self.on_finish, #
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                if msg_type == 'carPositions':
                    if 'gameTick' in msg:
                        self.on_car_positions(data, msg['gameTick'])
                    else:
                        self.on_car_positions(data, self.game_tick)
                else:
                    print(str(msg))
                    msg_map[msg_type](data)
            else:
                print(str(msg))
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = Bot(s, name, key)
        bot.run()
